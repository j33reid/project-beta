# CarCar

Team:


* Person 1 - Donovan, sales, automobile form and lists
* Jason Anderson - Service microservice


## Design


## Service microservice

I am starting with a complete database, I will be building onto a django project with  python on the backend using a Restful API puller. Then i'll be using React and Javascript on the frontend.  I will be focusing on the service side, creating technicians and appointments.

## Sales microservice

Using polling and Docker, I will be making a django application utilizing React, python, Json, Javascript, HTML, and css. This website will allow someone to manage their car dealership sales, with an inventory of cars, and a history of purchases and customers.
