import React, { useEffect, useState } from 'react';


export default function SalespersonForm() {
    const [salespeople, setSalespeople] = useState([]);
    
    
    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
    }
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const [employeeId, setEmployeeId] = useState('');
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value)
    }


    useEffect(() => {
        fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data ={}

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId

        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();
            console.log(newSalesperson)

            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="card-title">Create Life</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control"/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="col">
                            <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange} value={lastName} placeholder="Last Name"  required type="text" id="model_name" name="model_name" className="form-control"/>
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEmployeeIdChange} value={employeeId} placeholder="employee_id" required type="number" id="employee_id" name="employee_id" className="form-control"/>
                                <label htmlFor="employee_id">Employee ID</label>
                            </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}



