import React, { useState, useEffect } from "react";

function SaleForm() {



function SaleForm() {



    const[automobiles, setAutomobiles] = useState([]);


    const[salespeople, setSalespeople] = useState([]);


    const[customers, setCustomers] = useState([]);


    const[automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };


    const[salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };


    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };


    const[price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };


    const handleSubmit = async (event) => {
        event.preventDefault();

        const autoData = {};
        const saleData = {};
        saleData.automobile = automobile;
        saleData.salesperson = salesperson;
        saleData.customer = customer;
        saleData.price = price;
        autoData.sold = true


        const saleUrl = "http://localhost:8090/api/sales/";


        const saleFetchConfig = {
            method: "post",
            body: JSON.stringify(saleData),
            headers: {
                "Content-Type": "application/json",
            },
        };


        const saleResponse = await fetch(saleUrl, saleFetchConfig);
        if (saleResponse.ok)  {


            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    };

    // const saleResponse = await fetch(saleUrl, saleFetchConfig);
    // // const autoResponse = await fetch(autoUrl, autoFetchConfig);
    //         if (saleResponse.ok)  {

    //             setAutomobile('');
    //             setSalesperson('');
    //             setCustomer('');
    //             setPrice('');
    //         }
    };


    const fetchData = async () => {
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const customersUrl = "http://localhost:8090/api/customers/";


        const automobileResponse = await fetch(automobileUrl);
        const salespeopleResponse = await fetch(salespeopleUrl);
        const customerResponse = await fetch(customersUrl);


        if(automobileResponse.ok && salespeopleResponse.ok && customerResponse.ok)
        {
            const automobileData = await automobileResponse.json();
            const salespeopleData = await salespeopleResponse.json();
            const customersData = await customerResponse.json();

            setAutomobiles(automobileData.autos);
            setSalespeople(salespeopleData.salespeople);
            setCustomers(customersData.customers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Getcha Money</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select
                                value={automobile}
                                onChange={handleAutomobileChange}
                                required id="vin"
                                name="vin"
                                className="form-select"
                            >
                                <option value="">Choose an automobile VIN</option>
								{automobiles.map((automobile) => {
                                    if (automobile.sold === false) {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.vin}
                                            </option>
                                        );
                                    }
								})}
							</select>
						</div>
						<div className="mb-3">
							<label htmlFor="salesperson">Salesperson</label>
							<select
								value={salesperson}
								onChange={handleSalespersonChange}
								required id="salesperson"
								name="salesperson"
								className="form-select"
							>
								<option value="">Choose a salesperson</option>
								{salespeople.map((salesperson) => {
									return (
										<option
											key={salesperson.employee_id}
											value={salesperson.employee_id}
										>
											{salesperson.first_name + " " + salesperson.last_name}
										</option>
									);
								})}
							</select>
						</div>
						<div className="mb-3">
							<label htmlFor="customer">Customer</label>
							<select
								value={customer}
								onChange={handleCustomerChange}
								required id="customer"
								name="customer"
								className="form-select"
							>

                                <option value="">Choose a customer</option>
                                {customers.map((customer) => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name + '' + customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="price">Price</label>
                            <input
                                value={price}
                                onChange={handlePriceChange}
                                placeholder="0"
                                required type="number"
                                name="price"
                                id="price"
                                className="form-control"
                            />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    );
}
export default SaleForm;
