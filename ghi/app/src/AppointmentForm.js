import React, { useEffect, useState } from 'react';

function AppointmentForm() {
    const[appointments, setAppointment] = useState([]);
    const[technicians, setTechnicians] = useState([]);

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [dateTime, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const [reason, setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const [status, setStatus] = useState('');
    const handleStatusChange = (event) => {
        const value = event.target.value;
        setStatus(value);
    }

    const [technician, setTechnician] = useState('');
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};
        data.customer = customer;
        data.date_time = dateTime;
        data.reason = reason;
        data.vin = vin;
        data.status = status;
        data.technician = technician;
        console.log(data)

        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(data)
        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment)


            setCustomer('');
            setDateTime('');
            setReason('');
            setVin('');
            setStatus('');
            setTechnician('');
        }
    }

    const fetchData = async () => {
        const Url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="mb-3">
                            <input onChange={handleDateTimeChange} value={dateTime} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date</label>
                        </div>
                        <div className="mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin number</label>
                        </div>
                        {/* <div className="mb-3">
                            <input onChange={handleStatusChange} value={status} placeholder="reason" required type="text" name="status" id="status" className="form-control" />
                            <label htmlFor="status">Status</label>
                        </div> */}
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a Technician</option>
                                {technicians.map((technician) => {
                                    return (
                                        <option key={technician.employee_id} value={technician.id}>
                                            {technician.first_name + " " + technician.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AppointmentForm;
