import React, { useState, useEffect } from "react";

function SalespersonHistory() {
    const[salespeople, setSalespeople] = useState([]);
    const[sales, setSales] = useState([]);
    const[salesperson, setSalesperson] = useState(0);

    const handleSalespersonChange = (event) => {
        const value = parseInt(event.target.value);
        setSalesperson(value);
    }

    const fetchData = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const salespeopleResponse = await fetch(salespeopleUrl);
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        }

        const salesUrl = "http://localhost:8090/api/sales/";
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        }
    }

    const filteredSales = () => {
        const filtered = sales.filter(sale => sale.salesperson.id === salesperson);
        return filtered;
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select
                    value={salesperson}
                    onChange={handleSalespersonChange}
                    required
                    id="salesperson"
                    name="salesperson"
                    className="form-select"
                >
                    <option value="">Choose a Salesperson</option>
                    {salespeople.map((salesperson) => {
                        return (
                            <option
                                key={salesperson.employee_id}
                                value={salesperson.employee_id}
                            >
                                {salesperson.first_name + " " + salesperson.last_name}
                            </option>
                        );   
                    })}
                </select>
            </div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales().map((sale) => {
                        return (
                            <tr key={sale.id} value={sale.id}>
                                <td>
                                    {sale.salesperson.first_name + " " + sale.salesperson.last_name}
                                </td>
                                <td>
                                    {sale.customer.first_name + " " + sale.customer.last_name}
                                </td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}.00</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalespersonHistory;