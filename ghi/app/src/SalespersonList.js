import React, { useEffect, useState } from 'react';




function SalespersonList() {

        
    const [salespeople, setSalespeople] = useState([])

    const fetchData = async () => {
        const Url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                { salespeople.map(salesperson => {
                    return (
                        <tr key={ salesperson.employee_id } value={ salesperson.employee_id }>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td>{ salesperson.employee_id }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default SalespersonList;