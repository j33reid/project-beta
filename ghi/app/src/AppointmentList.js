import React, { useEffect, useState } from "react"

function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const [vins, setVins] = useState([])

    const cancelAppointment = async(id) => {
        const CancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const status={"status":"cancel"}
        const fetchConfig = {
          method:"PUT",
          body:JSON.stringify(status),
          headers:{
            'Content-Type':'application/json'
          }
        }
        const cancelResponse = await fetch(CancelUrl, fetchConfig);
        if (cancelResponse.ok) {
            fetchData();

        }
    }

    const finishAppointment = async(id) => {
        const FinishUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const status={"status":"finish"}
        const fetchConfig = {
          method:"PUT",
          body:JSON.stringify(status),
          headers:{
            'Content-Type':'application/json'
          }
        }
        const finishResponse = await fetch(FinishUrl, fetchConfig);
        if (finishResponse.ok) {
            fetchData();

        }
      }

    const fetchData = async () => {
        const Url = "http://localhost:8080/api/appointments/";
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAppointments(data.appointment)
        }
        const VinUrl = "http://localhost:8100/api/automobiles/";
        const vinResponse = await fetch(VinUrl);
        if (vinResponse.ok) {
            const vinData = await vinResponse.json();
            const vinList = []
            console.log(vinData)
            for(let automobile of vinData.autos){
              vinList.push(automobile.vin)
            }
            console.log(vinList)
            setVins(vinList)
        }

    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
      appointments.length > 0 ?
      <table className="table table-striped">
        <thead>
        <h1>Service Appointments</h1>
          <tr>
            <th>Customer</th>
            <th>Date and Time</th>
            <th>Reason</th>
            <th>VIN</th>
            <th>Is vip?</th>
            <th>Status</th>
            <th>Technician Name</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{ appointment.customer }</td>
                <td>{ appointment.date_time }</td>
                <td>{ appointment.reason }</td>
                <td>{ appointment.vin }</td>
                <td>{ vins.includes(appointment.vin)? "yes" : "no" }</td>
                {/* <td>{ appointment.vip_status }</td> */}
                <td>{ appointment.status }</td>
                <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                <td><button onClick={() => cancelAppointment(appointment.id)} className="btn-sm btn-danger">Cancel</button></td>
                <td><button onClick={() => finishAppointment(appointment.id)} className="btn-xs btn-success">Finish</button></td>
            </tr>
            );
          })}
        </tbody>
      </table> : <p>No appointments</p>

    );
  }

  export default AppointmentList;
