import React, { useEffect, useState } from 'react';

export default function TechnicianForm() {
    const [technician, setTechnician] = useState([]);

    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const [technicianId, setTechnicianId] = useState('');
    const handleTechnicianIdChange = (event) => {
        const value = event.target.value;
        setTechnicianId(value)
    }

    const fetchData = async () => {
      const url = "http://localhost:8080/api/technicians/"
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setTechnician(data.technician);
      }
  }

    useEffect(() => {
      fetchData();
    }, []);

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = technicianId;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
          const newTechnician = await response.json();

          setFirstName('');
          setLastName('');
          setTechnicianId('');
        }
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create new Technician</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} value={firstName} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} value={lastName} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTechnicianIdChange} value={technicianId} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee Id</label>
              </div>
              <button className="btn btn-lg btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
