import React, { useEffect, useState } from 'react';

export default function ManufacturerForm() {
    const [manufacturer, setManufacturer] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};

        data.name = name;

        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturersUrl, fetchConfig);
        if (response.ok) {

          setName('');
        }
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create new Manufacturer</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Manufacturer Name</label>
              </div>
              <button className="btn btn-lg btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
