import React, { useEffect, useState } from 'react';

export default function ModelsForm() {
    const [models, setModels] = useState([]);
    const [manufacturers, setManufacturers] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [manufacturer_id, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const [picture_url, setPictureUrl] = useState('');
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const modelData = {};

        modelData.name = name;
        modelData.picture_url = picture_url;
        modelData.manufacturer_id = manufacturer_id;

        const modelsUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(modelData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(modelData)
        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {
          const newModel = await response.json();
          console.log(newModel)

          setName('');
          setPictureUrl('');
          setManufacturer('');
        }
    }

    const fetchData = async () => {
      const url = "http://localhost:8100/api/manufacturers/"
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
  }

    useEffect(() => {
      fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create new Model</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={picture_url} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Model picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer_id} required name="manufacturer" id="manufacturer" className="form-select">
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map((manufacturer) => {
                        return (
                            <option key={manufacturer.name} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
