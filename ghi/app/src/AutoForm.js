import React, { useState, useEffect } from "react";

function AutoForm()  {
    const [models, setModels] = useState([]);

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const [year, setYear] = useState('');
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    };

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const [model, setModel] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const autoUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    };

    const fetchData = async () => {
        const modelsUrl = "http://localhost:8100/api/models/";

        const modelsResponse = await fetch(modelsUrl);

        if (modelsResponse.ok) {
            const modelsData = await modelsResponse.json();
            setModels(modelsData.models);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Car</h1>
					<form onSubmit={handleSubmit} id="create-auto-form">
						<div className="mb-3">
							<input
								value={color}
								onChange={handleColorChange}
								placeholder="Color"
								required
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<input
								value={year}
								onChange={handleYearChange}
								placeholder="Year"
								required
								type="text"
								name="year"
								id="year"
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<input
								value={vin}
								onChange={handleVinChange}
								placeholder="VIN"
								required
								type="text"
								name="vin"
								id="vin"
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<select
								value={model}
								onChange={handleModelChange}
								required
								id="model"
								name="model"
								className="form-select"
							>
								<option value="">Choose a vehicle model</option>
								{models.map((model) => {
									return (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}
export default AutoForm;
