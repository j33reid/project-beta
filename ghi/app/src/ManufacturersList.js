import React, { useEffect, useState } from "react";

function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState([])

    const fetchData = async () => {
        const Url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
               {manufacturers.map(manufacturer => {
                    return (
                        <tr key={ manufacturer.id } value={ manufacturer.id }>
                            <td>{ manufacturer.name }</td>
                        </tr>
                        );
                    })}
            </tbody>
        </table>
    );
  }

  export default ManufacturerList;
