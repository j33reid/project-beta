import React, { useEffect,useState } from 'react';

export default function CustomerForm() {

    const [customers, setCustomers] = useState([]);

    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const[lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    }

    const [address, setaddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setaddress(value)
    }

    const [phoneNumber, setPhoneNumber] = useState('');
    const handlephoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/"
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customer);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();

            setFirstName('');
            setLastName('');
            setaddress('');
            setPhoneNumber('');
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Uninformed Money Provider</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={lastName} placeholder="Last Name" required type="text" id="last_name" name="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} value={address} placeholder="Address" required type="text" id="address" name="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlephoneNumberChange} value={phoneNumber} placeholder="Phone Number" required type="number" id="phone_number" name="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
