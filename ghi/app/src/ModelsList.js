import React, { useEffect, useState } from "react";

function ModelsList() {
    const [models, setModels] = useState([])

    const fetchData = async () => {
        const Url = "http://localhost:8100/api/models/";
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
               {models.map(models => {
                    return (
                        <tr key={ models.name } value={models.name}>
                            <td>{ models.name }</td>
                            <td>{ models.manufacturer.name }</td>
                            <td>{ models.picture_url }</td>
                        </tr>
                        );
                    })}
            </tbody>
        </table>
    );
  }

  export default ModelsList;
