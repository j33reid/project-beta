import React, { useEffect, useState } from "react";


function ServiceHistory(props) {
    const [appointments, setAppointment] = useState([])
    const [automobiles, setAutomobiles] = useState([])


    const [query, setquery] = useState('')

    // const handleChange = (e) => {
    //     const results = automobiles.filter(automobile => {
    //         if (e.target.value === "") return automobiles
    //         return automobile.vin.includes(e.target.value)
    //     })
        // setquery({
        //     query: e.target.value,
        //     autolist: results
        // })
    



    const fetchData = async () => {
        const Url = 'http://localhost:8080/api/appointments';
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointment)
        }
        const AutomobileUrl = "http://localhost:8100/api/automobiles/";
        const autoResponse = await fetch(AutomobileUrl);
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            const autoList = []
            console.log(autoData)
            for(let automobile of autoData.autos){
              autoList.push(automobile.vin)
            }
            console.log(autoList)
            setAutomobiles(autoList)
            // const filteredVins = search.length === 0 ? vinList :
            // vinList.filter(vinList => vinList.vin.includes(search()))
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
        <div>

            <form>
                <input onChange={(e) => setquery(e.target.value) } type="search"/>
            </form>
            <ul>
                {/* {(automobiles.query === '' ? "No VIN's match search" : automobiles.map(automobile => {
                return <li key={automobile.vin}>{automobile.vin}</li>
            }))} */}
            </ul>
        </div>
        <table className="table table-striped">
            <thead>

                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date/Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
               {appointments.filter((appointment) =>
               appointment.vin.includes(query)).map(appointment => {
                    return (
                        <tr key={ appointment.id } >
                            <td>{ appointment.vin }</td>
                            {/* <td>{ vins.includes(appointment.vin)? "yes" : "no" }</td> */}
                            <td>{appointment.vip_status }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date_time }</td>
                            <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                        );
                    })}
            </tbody>
        </table>
        </>
    );
}

  export default ServiceHistory;
