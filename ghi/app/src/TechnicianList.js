import React, { useEffect, useState } from "react";

function TechnicianList() {
    const [technician, setTechnician] = useState([])

    const fetchData = async () => {
        const Url = "http://localhost:8080/api/technicians/";
        const response = await fetch(Url);
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
               {technician.map(technician => {
                    return (
                        <tr key={ technician.employee_id }>
                            <td>{ technician.employee_id }</td>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                        </tr>
                        );
                    })}
            </tbody>
        </table>
    );
  }

  export default TechnicianList;
