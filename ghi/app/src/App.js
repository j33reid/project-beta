import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AutoList from './AutoList';
import AutoForm from './AutoForm';

import ManufacturerForm from './ManufacturersForm';
import ManufacturerList from './ManufacturersList';
import ServiceHistory from './ServiceHistory';
import ModelsForm from './ModelsForm';
import ModelsList from './ModelsList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople" element={<SalespersonList  />} />
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />} />
            <Route path="history" element={<SalespersonHistory  />} />
          </Route>
          <Route path="customers" element={<CustomerList />} />
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales" element={<SalesList />} />
          <Route path="sales">
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="technicians" element={<TechnicianList  />} />
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments" element={<AppointmentList  />} />
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route path="automobiles" element={<AutoList />} />
          <Route path="automobiles">
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="maufacturers">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" element={<ModelsList models />} />
          <Route path="models">
            <Route path="new" element={<ModelsForm />} />
          </Route>
          <Route path="servicehistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
