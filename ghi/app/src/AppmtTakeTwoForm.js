import React, { useEffect, useState } from 'react';


function AppointmentForm() {
    const[appointments, setAppointment] = useState([]);
    const[technicians, setTechnicians] = useState([]);

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [dateTime, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const [reason, setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const [status, setStatus] = useState('');
    const handleStatusChange = (event) => {
        const value = event.target.value;
        setStatus(value);
    }

    const [technician, setTechnician] = useState('');
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setAppointment(data.appointments);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

