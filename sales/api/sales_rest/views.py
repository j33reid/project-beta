from django.shortcuts import get_object_or_404
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

from .models import Sale, Salesperson, AutomobileVO, Customer
from .encoders import SalespersonEncoder, SaleEncoder, CustomerEncoder


@require_http_methods(["GET", "POST"])
def api_salespeople_list(request):

    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_salesperson(request, pk):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Ain't Here"})
    

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_customer(request, pk):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "They got snapped"})


@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            customer_id = content["customer"]
            salesperson_id = content["salesperson"]
            customer = Customer.objects.get(id=customer_id)
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            vin = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = vin
            content["salesperson"] = salesperson
            content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': "Aint no car"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        sale = get_object_or_404(Sale, id=id)
        count, _ = sale.delete()
        return JsonResponse({"deleted": count > 0})