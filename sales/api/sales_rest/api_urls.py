from django.urls import path
from .views import (api_salespeople_list,
                    api_salesperson,
                    api_customer_list,
                    api_customer,
                    api_sales_list,
                    api_delete_sale)

urlpatterns = [
    path("sales/<int:pk>/", api_delete_sale, name="api_delete_sale"),
    path("sales/", api_sales_list, name="api_sales_list"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("salespeople/<int:pk>/", api_salesperson, name="api_salesperson"),
    path("salespeople/", api_salespeople_list, name="api_salespeople_list"),
]