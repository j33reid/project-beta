from django.urls import path
from .views import (
    api_technicians,
    api_technician,
    api_appointment,
    api_appointments,
    api_cancel,
    api_finish,
)


urlpatterns = [
    path("appointments/<int:pk>/finish/", api_finish, name="api_finish"),
    path("appointments/<int:pk>/cancel/", api_cancel, name="api_cancel"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
]
