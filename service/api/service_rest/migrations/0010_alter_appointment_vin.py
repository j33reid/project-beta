# Generated by Django 4.0.3 on 2023-05-01 21:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0009_delete_status_appointment_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='vin',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
