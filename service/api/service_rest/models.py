from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    import_href = models.CharField(max_length=100, default=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ("last_name",)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=100, null=True)
    vip_status = models.CharField(max_length=100, null=True)
    customer = models.CharField(max_length=200)
    status = models.CharField(max_length=100, default="created")
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("technician", "customer")
