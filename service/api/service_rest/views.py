from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Technician, Appointment
from .encoders import (AppointmentDetailEncoder, AppointmentListEncoder,
                       TechnicianEncoder)


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()

        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:

        content = json.loads(request.body)
        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
            )


@require_http_methods(["DELETE"])
def api_technician(request, pk):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()

            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()

        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        content["status"] = "created"
        try:
            technician = content["technician"]
            technician = Technician.objects.get(id=technician)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid technician id"},
                status=400,
            )

        vin = content["vin"]
        try:
            auto = AutomobileVO.objects.get(vin=vin)
            print(auto)
            if auto.vin == vin:
                content['vip_status'] = "yes"
        except AutomobileVO.DoesNotExist:
            content['vip_status'] = "no"

        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_appointment(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=pk).delete()

            return JsonResponse(
                {"deleted": count > 0})

        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

    elif request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "There is no Appointment"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_cancel(request, pk):
    content = json.loads(request.body)
    try:
        appointment = Appointment.objects.get(id=pk)
        content["status"] = "CANCELED"

    except Appointment.DoesNotExist:
        return JsonResponse(
            {"error": "There is no Appointment"},
            status=400,
        )
    Appointment.objects.filter(id=pk).update(**content)
    appointment = Appointment.objects.get(id=pk)

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_finish(request, pk):
    content = json.loads(request.body)
    try:
        appointment = Appointment.objects.get(id=pk)
        content["status"] = "FINISHED"

    except Appointment.DoesNotExist:
        return JsonResponse(
            {"error": "There is no Appointment"},
            status=400,
        )

    Appointment.objects.filter(id=pk).update(**content)
    appointment = Appointment.objects.get(id=pk)

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
