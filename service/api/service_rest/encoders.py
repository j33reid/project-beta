from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "date_time",
        "reason",
        "vin",
        "vip_status",
        "technician",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "date_time",
        "reason",
        "vin",
        "vip_status",
        "technician",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
